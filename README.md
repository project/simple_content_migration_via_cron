# Simple content migration via cron scheduler

The module extends the core migration system to migrate the
content from external database as source into Drupal nodes as destination.

### Features of the module:
* Migrate the content form external database source into Drupal nodes.
* Migration of the content will be performed using hook_cron().
* It requires contrib modules `migrate_plus`
  (https://www.drupal.org/project/migrate_plus).

### Installation Steps

1. Download the module(simple_content_migration_via_cron) and 
  dependencies module (migrate_plus).

2. Enable the modules - migrate_plus and simple_content_migration_via_cron.

3. Add the external database(to be migrated) config in active settings.php file.
    ```sh
      $databases['migrate']['default'] = array (
        'database' => 'source_db',
        'username' => 'root',
        'password' => '',
        'prefix' => '',
        'host' => '127.0.0.1',
        'port' => '3306',
        'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
        'driver' => 'mysql',
      );
    ```

4. Add migration scheduler config in settings.php file.

  ```sh
    $config['simple_content_migration_via_cron']['migrations'] = [
      'content_migration' => [
        'time' => 3600, # To be executed every hour.
        'update' => TRUE, # To be executed with the --update flag.
      ],
    ];
  ```

5. Choose the scheduler time as mentioned in step `4` from Cron dropdown
   field (admin/config/system/cron) and save. Cron API which is built into
   the Drupal core is used to schedule the migrations.

6. Source database fields content to be migrated:
    * title (plain text)
    * sku (plain text - primary key)
    * price (plain text)
    * valid_date (datetime)

7. Destination database(e.g. Drupal) fields of the content type `product`:
    * title (plain text)
    * field_sku (plain text)
    * field_price (plain text)
    * field_valid_date (datetime)

  Note: If you have different machine name of content type, make sure to
   change the default_bundle in the file
   simple_content_migration_via_cron/config/install/migrate_plus.migration.content_migration.yml

8. Change the source fields in the source plugin - `Content.php`

9. Map the fields in the file migrate_plus.migration.content_migration.yml

10. Content will be migrated as per scheduled in the cron tab.
