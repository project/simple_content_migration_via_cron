<?php

namespace Drupal\simple_content_migration_via_cron\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * Source plugin for content migration.
 *
 * @MigrateSource(
 *   id="content_migration",
 *   source_module = "simple_content_migration_via_cron",
 * )
 */
class Content extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Source data is queried from 'products' table.
    $query = $this->select('products', 'p');
    $query->fields('p', [
      'title',
      'sku',
      'price',
      'valid_date',
    ]);

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = $this->baseFields();

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function baseFields() {
    $fields = [
      'title' => $this->t('Title'),
      'sku' => $this->t('SKU'),
      'price' => $this->t('Price'),
      'valid_date' => $this->t('Product Valid Date'),
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'sku' => [
        'type' => 'string',
        'alias' => 'p',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $valid_date = $row->getSourceProperty('valid_date');
    // Explode if date and time is separated with a space.
    $valid_date_arr = explode(' ', $valid_date);
    $datetime = $valid_date_arr[0] . 'T' . $valid_date_arr[1];
    $row->setSourceProperty('validDate', $datetime);

    return parent::prepareRow($row);
  }

}
